use clap::Parser;

/// Dilates the specified PNG image.
/// In-place, if no OUT file was specified.
#[derive(Parser)]
struct Cli {
    image: std::path::PathBuf,
    out: Option<std::path::PathBuf>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let cli: Cli = Cli::parse();

    let data = std::io::Cursor::new(std::fs::read(&cli.image)?);
    let reader = image::io::Reader::with_format(data, image::ImageFormat::Png);
    let mut image = reader.decode()?.into_rgba8();

    twmap::edit::dilate(&mut image);

    let out_path = cli.out.as_ref().unwrap_or(&cli.image);
    image.save(out_path)?;
    Ok(())
}
